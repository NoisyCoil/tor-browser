# This image is published in containers.torproject.org/tpo/applications/tor-browser/base
#
# Whenever there are changes to this file,
# they are autopublished on merge to the tpo/applications/tor-browser repository.
#
# The image is updated roughly once a monce when the tor-browser repository is rebased.

FROM containers.torproject.org/tpo/tpa/base-images/python:bookworm

RUN apt-get update && apt-get install -y \
        clang \
        curl \
        git \
        libasound2-dev \
        libdbus-glib-1-dev \
        libgtk-3-dev \
        libpango1.0-dev \
        libpulse-dev \
        libx11-xcb-dev \
        libxcomposite-dev \
        libxcursor-dev \
        libxdamage-dev \
        libxi-dev \
        libxrandr-dev \
        libxtst-dev \
        make \
        m4 \
        mercurial \
        nasm \
        pkgconf \
        unzip \
        x11-utils \
        xvfb \
        xz-utils \
        wget && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY taskcluster/docker/recipes/install-node.sh ./install-node.sh
RUN chmod +x install-node.sh
RUN ./install-node.sh
RUN rm ./install-node.sh

COPY taskcluster/kinds/fetch/toolchains.yml ./toolchains.yml
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain $(grep -oP 'rust-\K[0-9.]+(?=:)' ./toolchains.yml)
RUN $HOME/.cargo/bin/cargo install cbindgen --version $(grep -oP 'cbindgen-\K[0-9.]+(?=:)' ./toolchains.yml)
RUN rm ./toolchains.yml

CMD ["/bin/bash"]
