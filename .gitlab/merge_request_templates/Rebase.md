## Merge Info

<!-- Bookkeeping information for release management -->

### Rebase Issue
- tor-browser#xxxxx
- mullvad-browser#xxxxx

### Release Prep Issue
- tor-browser-build#xxxxx

### Issue Tracking
- [ ] Link rebase issue with appropriate [Release Prep issue](https://gitlab.torproject.org/groups/tpo/applications/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=Release%20Prep&first_page_size=20) for changelog generation

### Review

#### Request Reviewer

- [ ] Request review from a release engineer: boklm, dan, ma1, morgan, pierov

#### Change Description

<!-- Any interesting notes about the rebase and an overview of what the reviewer should expect from the diff of diffs and range-diff -->
